//
//  AddressViewController.swift
//  GhostProfileGen
//
//  Created by Xiaodan Wang on 5/30/20.
//  Copyright © 2020 Xiaodan Wang. All rights reserved.
//

import UIKit

class AddressViewController: BaseViewController {
 
    class AddressParser: ClipboardParser
    {
        override func parse(content: String) -> [DataModel] {
            guard !content.isEmpty else { return [] }
            let lines = content.components(separatedBy: CharacterSet(charactersIn: ";\t"))
            return lines.map { line -> AddressDataModel? in
                let words = line.components(separatedBy: ":")
                if words.count == 7 {
                    return AddressDataModel(fn: words[0], ln: words[1], ad1: words[2], ad2: words[3], z: words[4], st: words[5], ph: words[6])
                } else if words.count == 6 {
                    return AddressDataModel(fn: words[0], ln: words[1], ad1: words[2], ad2: "", z: words[3], st: words[4], ph: words[5])
                } else {
                    return nil
                }
            }.compactMap { $0 }
        }
    }
    
    class AddressDataModel: DataModel, Equatable, Hashable
    {
        static func == (lhs: AddressViewController.AddressDataModel, rhs: AddressViewController.AddressDataModel) -> Bool {
            return lhs.fn == rhs.fn && lhs.ln == rhs.ln && lhs.ad1 == rhs.ad1 && lhs.ad2 == rhs.ad2 && lhs.z == rhs.z && lhs.st == rhs.st && lhs.ph == rhs.ph
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(fn)
            hasher.combine(ln)
            hasher.combine(ad1)
            hasher.combine(ad2)
            hasher.combine(z)
            hasher.combine(st)
            hasher.combine(ph)
        }
        
        let fn:String
        let ln:String
        let ad1:String
        let ad2:String
        let z:String
        let st:String
        let ph:String

        init(fn:String, ln:String, ad1:String, ad2:String, z:String, st:String, ph:String) {
            self.fn = fn
            self.ln = ln
            self.ad1 = ad1
            self.ad2 = ad2
            self.z = z
            self.st = st
            self.ph = ph
            
            super.init()
        }
        
        required init(from decoder: Decoder) throws {
            fatalError("init(from:) has not been implemented")
        }
    }
    
    // Mark: sub class overriden
    
    override var parser: ClipboardParser? {
        // Fn:Ln:Ad1:Ad2(?):Z:St:Ph;
        return AddressParser()
    }
        
    // IB
    
    @IBAction func updateButtonTapped(_ sender: Any) {
    }
    
    @IBAction func saveAsNewProfileButtonTapped(_ sender: Any) {
    }
    
    var addressDataModels = [AddressDataModel]()
    var addressDataModelsSet = Set<AddressDataModel>()
    
    override func viewWillAppear(_ animated: Bool) {
        guard isBeingPresented else { return super.viewWillAppear(animated) }
        
        // subscribe to clipboard
        _ = NotificationCenter.default
            .publisher(for: NotificationCenter.ClipboardDataModelsParsed)
            .debounce(for: .milliseconds(1000), scheduler: RunLoop.main)
            .receive(on: RunLoop.main)
            .map {
                $0.object as! [DataModel]
            }
            .map { dataModels -> [AddressDataModel] in
                dataModels.compactMap({ ($0 as? AddressDataModel) })
            }
            .removeDuplicates()
            .map { dataModels -> [AddressDataModel] in
                dataModels.filter({ self.addressDataModelsSet.contains($0) })
            }
            .sink {
                print($0)
                self.addressDataModels += $0
                self.addressDataModelsSet = Set(self.addressDataModels)
            }
        
        // subscribe to local data loading
        _ = NotificationCenter.default
            .publisher(for: NotificationCenter.LocalDataModelsLoaded)
            .receive(on: RunLoop.main)
            .map {
                $0.object as! [DataModel]
            }
            .map { dataModels -> [AddressDataModel] in
                dataModels.compactMap({ ($0 as? AddressDataModel) })
            }
            .map { dataModels -> [AddressDataModel] in
                dataModels.filter({ self.addressDataModelsSet.contains($0) })
            }
            .removeDuplicates()
            .sink {
                print($0)
                self.addressDataModels += $0
                self.addressDataModelsSet = Set(self.addressDataModels)
            }
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}


