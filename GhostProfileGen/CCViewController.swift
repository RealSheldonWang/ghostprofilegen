//
//  CCViewController.swift
//  GhostProfileGen
//
//  Created by Xiaodan Wang on 6/2/20.
//  Copyright © 2020 Xiaodan Wang. All rights reserved.
//

import UIKit

class CCViewController: BaseViewController {

    class CCParser: ClipboardParser
    {
        override func parse(content: String) -> [DataModel] {
            return []
        }
    }
    
    class CCDataModel: DataModel
    {
        
    }
    
    override var parser: ClipboardParser? {
        return CCParser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
