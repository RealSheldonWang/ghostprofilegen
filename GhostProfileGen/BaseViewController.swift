//
//  BaseViewController.swift
//  GhostProfileGen
//
//  Created by Xiaodan Wang on 5/30/20.
//  Copyright © 2020 Xiaodan Wang. All rights reserved.
//

import UIKit
import Combine

class BaseViewController: UIViewController {
    
    // designed to be overriden by sub classes
    var parser: ClipboardParser? {
        return nil
    }
    
    lazy var parserQueue: DispatchQueue = DispatchQueue(label: self.description)
    
    var dataPublisher: AnyPublisher<DataModel, Error>?
    
    @IBAction func dismissKeyboard(_ sender: Any) {
        // Connected to every button
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _parseAndPublish()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appbecameActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        let keyboardHeight: CGFloat = keyboardViewEndFrame.height
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.view.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height);
        } else {
            self.view.frame = CGRect(x:0, y:-keyboardHeight+self.view.safeAreaInsets.bottom, width:self.view.frame.width, height:self.view.frame.height);
        }
    }

    @objc func appbecameActive(notification: Notification) {
        self.view.endEditing(true)
        _parseAndPublish()
    }
    
    func _parseAndPublish() {
        guard
            let parser = parser,
            UIPasteboard.general.hasStrings,
            let content = UIPasteboard.general.string
        else { return }
        parserQueue.async {
            let dataModels = parser.parse(content: content)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NotificationCenter.ClipboardDataModelsParsed, object: dataModels, userInfo: nil)
            }
        }
    }
    
}
