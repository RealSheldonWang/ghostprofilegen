//
//  EmailViewController.swift
//  GhostProfileGen
//
//  Created by Xiaodan Wang on 5/30/20.
//  Copyright © 2020 Xiaodan Wang. All rights reserved.
//

import UIKit

class EmailViewController: BaseViewController {
    
    class EmailParser: ClipboardParser
    {
        override func parse(content: String) -> [DataModel] {
            return []
        }
    }
    
    class EmailDataModel: DataModel
    {
        
    }
    
    override var parser: ClipboardParser? {
        return EmailParser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

