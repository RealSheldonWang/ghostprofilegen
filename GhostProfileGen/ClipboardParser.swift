//
//  ClipboardParser.swift
//  GhostProfileGen
//
//  Created by Xiaodan Wang on 6/2/20.
//  Copyright © 2020 Xiaodan Wang. All rights reserved.
//

import UIKit

class ClipboardParser: NSObject {
        
    func parse(content: String) -> [DataModel] {
        assert(false, "need to be overriden with implementation")
        return []
    }
}

class DataModel: Codable { }

extension NotificationCenter {
    public static let ClipboardDataModelsParsed = Notification.Name("cdmp")
}
